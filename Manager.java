package Assignments.seven;



public class Manager extends Employee{
    int fixedSalary;
    int incentive;
    int salary;

    Manager(String name,int id)
    {
        this.eName=name;
        this.eId=id;
    }
    public void computeSalary()
    {
        System.out.println("Enter the fixed salary of the Manager");
        fixedSalary= sc.nextInt();
        System.out.println("Enter the incentive for manager");
        incentive=sc.nextInt();
        salary=fixedSalary+incentive;

    }

    public String toString()
    {
        return("The salary of the manager with the incentive "+incentive+" and fixed Salary is "+fixedSalary+" is "+salary);
    }

}
