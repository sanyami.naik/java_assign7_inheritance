package Assignments.seven;
import java.util.Scanner;


public  class TestEmployee{
    public static void main(String[] args) {

        Scanner sc=new Scanner(System.in);
        String wageName;
        int wageId;
        System.out.println("Enter the name of Wage employee");
        wageName= sc.nextLine();
        System.out.println("Enter the ID of Wage employee");
        wageId= sc.nextInt();
        WageEmployee wageEmployee=new WageEmployee(wageName,wageId);
        wageEmployee.computeSalary();
        System.out.println(wageEmployee);


        System.out.println("===========================================================");
        String salesName;
        int salesId;
        System.out.println("Enter the name of Salesperson");
        sc.nextLine();
        salesName= sc.nextLine();
        System.out.println("Enter the ID of Salesperson");
        salesId= sc.nextInt();
        SalesPerson salesPerson=new SalesPerson(salesName,salesId);
        salesPerson.computeSalary();
        System.out.println(salesPerson);


        System.out.println("===========================================================");
        String managerName;
        int managerId;
        System.out.println("Enter the name of manager");
        sc.nextLine();
        managerName= sc.nextLine();
        System.out.println("Enter the ID of manager");
        managerId= sc.nextInt();
        Manager manager=new Manager(managerName,managerId);
        manager.computeSalary();
        System.out.println(manager);

        System.out.println("=============================================================================");
        System.out.println("The Arrray of all Employees is as below");
        Employee array[]=new Employee[3];
        array[0]=wageEmployee;
        array[1]=salesPerson;
        array[2]=manager;

        for (Employee e:array) {
            System.out.println(e);
        }







    }
}





/*
        OUTPUT:

        Enter the name of Wage employee
        Ram Mahajan
        Enter the ID of Wage employee
        111
        Enter the hrs of employee
        34
        Enter the rate per hour
        1200
        The name of the Wage employee is Ram Mahajan with Id 111.The salary of this Wage employee at the rate 200 is 40800
        ===========================================================
        Enter the name of Salesperson
        Vidhita Joshi
        Enter the ID of Salesperson
        222
        Enter the sales done by the salesperson
        56
        Enter the rate per hour
        1000
        The name of the salesperson is Vidhita Joshi with Id 222.The salary of Salesperson at the commission 1000 is 56000
        ===========================================================
        Enter the name of manager
        Vishal Pandit
        Enter the ID of manager
        333
        Enter the fixed salary of the Manager
        72030
        Enter the incentive for manager
        34000
        The salary of the manager with the incentive 34000 and fixed Salary is 72030 is 106030
        =============================================================================
        The Arrray of all Employees is as below
        The name of the Wage employee is Ram Mahajan with Id 111.The salary of this Wage employee at the rate 200 is 40800
        The name of the salesperson is Vidhita Joshi with Id 222.The salary of Salesperson at the commission 1000 is 56000
        The salary of the manager with the incentive 34000 and fixed Salary is 72030 is 106030

*/